#!/bin/bash
CC = gcc
RM = rm -f

all:
	$(CC) pdg.c -O3 -o pdg -lm


clean:
	$(RM) *.o pdg
